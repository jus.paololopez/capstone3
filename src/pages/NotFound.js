import React from 'react'

import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';

export default function NotFound() {

    return (
        

        <Container>

             <h1>404 Page Not Found</h1>
            <Link to="/">Go Home</Link>
            
        </Container>
           
       
    )
}

