import React, { useState, useEffect, useContext } from 'react'
import { Table, Jumbotron } from 'react-bootstrap';
import Product from '../components/Product';

import UserContext from '../UserContext';


export default function MyOrders({userId,productId}) {

    
const { user } = useContext(UserContext);
	console.log(user)

const [myOrders, setMyOrders] = useState([]);


useEffect(() => {

	
}, [])
		fetch(`https://secret-tundra-31885.herokuapp.com/users/${userId}/myOrder`, {
				headers: { 
						Authorization: `Bearer ${user.accessToken}` 
				}
			})
			.then(response => response.json())
			.then(data =>{
			console.log(data);

			setMyOrders(data.order.map(orders=>{

				return (

				 	<tr key={orders._id}>
					<td>{orders.productId._id}</td>
					<td>{orders.productId.name}</td>
					<td>{orders.productId.description}</td>
					<td>{orders.productId.price}</td>
					
					</tr>

						
							)

			}))
				
				})
						
			

			
		return (

		<>

			<Jumbotron className="mt-5">
			
				<h1 className="text-center mb-5" ><strong>My Orders</strong></h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							
						</tr>
					</thead>
					<tbody>
							{myOrders}
					</tbody>
				</Table>
			</Jumbotron>
		</>

		)
	}


// setMyOrders(data.map(viewOrders =>{
				// 	let orders = viewOrders.order;
				// 	if(orders.length){
						
				// 		return(
				// 			orders.map(orders=>{

				// 					return (

				// 						<tr key={orders._id}>
				// 						<td>{orders.productId}</td>	 
				//  						</tr>
						
				// 					)
				// 			})
				// 		)
				// 	}
					

				// 	}))