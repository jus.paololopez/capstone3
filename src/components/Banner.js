import React from 'react';

import { Jumbotron, Button, Row, Col } from 'react-bootstrap';
import { useHistory } from 'react-router';
import banner1 from '../images/new/banner.jpg';
import '../App.css';
export default function Banner() {

 const history = useHistory();
    return(
        <>
        <h1>Let's Play and Restart</h1>
        <h2>Pick your poison</h2>
    <Row>
        <Col>
            <img src= { banner1 } className="banner" />
        </Col>
    </Row>
    </>
    )
}