import React, { Fragment, useContext } from 'react';
//import necessary components
import Navbar from 'react-bootstrap/Navbar';//export default = it can be change or placed it on any variable we want
import { Nav } from 'react-bootstrap';
import { Link, NavLink, useHistory } from 'react-router-dom';
import UserContext from '../UserContext'

import { Dropdown, DropdownButton, ButtonToolbar, ButtonGroup } from 'react-bootstrap'; 
export default function NavBar() {


//push(path) pushes a new entry onto the history stack
	const history = useHistory();

	//useContext() is a react hook used to unwrap our context. It will return the data passed as values by a provider
	const { user, setUser, unsetUser } = useContext(UserContext);
	console.log(user)


	const logout = () =>{
		unsetUser();
		setUser({ accessToken: null });
		history.push('/login')
	}

let rightNav = (user.accessToken === null) ? // if user logged in the logout option will be visible
<Fragment>
<Nav.Link as= {NavLink} to = "/login" className="links">Login</Nav.Link>
<Nav.Link as= {NavLink} to = "/register" className="links">Register</Nav.Link>
</Fragment>
:
(user.isAdmin) ?
<Fragment>
<Nav.Link as= {NavLink} to = "/" className="links">Home</Nav.Link>
<Nav.Link as= {NavLink} to = "/products" className="links">Graphics Card</Nav.Link>
<Nav.Link as= {NavLink} to = "/addProduct" className="links">Add Product</Nav.Link> 
<Nav.Link onClick={logout} className="links">Logout</Nav.Link>
</Fragment>
:

<Fragment>

<Nav.Link as= {NavLink} to = "/myOrder" className="links">My Orders</Nav.Link>
<Nav.Link onClick={logout} className="links">Logout</Nav.Link>
</Fragment>



    return(
        <Navbar bg = "light" expand="lg">
                <Navbar.Brand as={Link} to = "/">BedRoom PC</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Fragment>
                    <Nav.Link as= {NavLink} to = "/" className="links">Home</Nav.Link>
                    <Nav.Link as= {NavLink} to = "/products" className="links">Products</Nav.Link>
                    </Fragment>
                        <Nav className = "ml-auto">
                        {rightNav}
                        </Nav>

                    </Navbar.Collapse>
                    
            </Navbar>
    )
}

