import React, { useState } from 'react';
import { Button, Modal,Table } from 'react-bootstrap';



// use props
export default function GetButton({product,productId}) {
  
    // const { description, price } = product;

    const [show, setShow] = useState(false);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

    const [getOne, setGetOne] = useState(null);

    function getProduct(){
        fetch(`https://secret-tundra-31885.herokuapp.com/products/${productId}`,{
        headers: {

            'Authorization' : `Bearer ${localStorage.getItem('accessToken')}`
        }
        })
        .then(res=>res.json())
        .then(data =>{
          console.log(data)
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)

        
            
         
           
        })
        handleShow()
    }
return (
 <>
      <Button variant="info" onClick={getProduct}>View</Button>

      <Modal show={show} onHide={handleClose}>
		        <Modal.Header closeButton>
		          <Modal.Title>Product Details</Modal.Title>
		        </Modal.Header>

                
		        <Modal.Body>

                <Table>
                <thead> 

                <tr>
                    <th>Name</th> 
                    <th>Description</th>
                    <th>Price</th>
                </tr>

                </thead>

                
                <tbody>
                {/* {getOne} */}
                 <tr>
                    <td>{name}</td>
                    <td>{description}</td>
                    <td>{price}</td>
                     </tr>
                </tbody>

                </Table>
                     
                 </Modal.Body>
				

		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleClose}>
		            Close
		          </Button>

                  </Modal.Footer>
		    </Modal>
 
 </>
   
    )
   
}
