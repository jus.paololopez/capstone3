import React from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';


import img3 from '../images/new/new.jpg';
import img4 from '../images/new/hot.jpg';
import img5 from '../images/new/onsale.jpg';
export default function Highlights(){
   return(

	<Row>
		<Col>
			<Card className="cardHighlight">
            <Card.Img variant="top" src= { img3 } className="img1"/>
				<Card.Body>
					<Card.Title>
						<h2 >NEW ARRIVALS</h2>
					</Card.Title>
					<Card.Text>
						Get one of the best graphics cards available to take on the most demanding games of today. You’ll be able to handle any game, as well as video editing, at high resolutions with one of these powering your computer. And, thanks to AMD’s Big Navi and Nvidia’s RTX 3000 series, getting a new generation GPU.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col>
			<Card className="cardHighlight">
            <Card.Img variant="top" src= { img5 } className="img1"/>
				<Card.Body>
					<Card.Title>
						<h2>ON SALE NOW</h2>
					</Card.Title>
					<Card.Text>
						Our end-of-season sale starts NOW. Stop by the shop this week for 15% off summer apparel and select accessories. Stock is limited!  Today, 1080p monitors are affordable—you can find plenty of 'em down around $100—and even a midrange video card can run hot games.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col>
			<Card className="cardHighlight">
            <Card.Img variant="top" src= { img4 } className="img1"/>
				<Card.Body>
					<Card.Title>
						<h2>HOT PICKS</h2>
					</Card.Title>
					<Card.Text>
						Nvidia's new designs for its Founders Edition cards throw most of the conventional wisdom out the window. The cards are packed onto a PCB ("printed circuit board," the guts of a GPU), that is 50% smaller than previous-generation RTX 20 Series Nvidia
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
	</Row>
   )
}